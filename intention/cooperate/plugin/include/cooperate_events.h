/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef COOPERATE_EVENTS_H
#define COOPERATE_EVENTS_H

#include <string>
#include <variant>

namespace OHOS {
namespace Msdp {
namespace DeviceStatus {
namespace Cooperate {
enum CooperateState : size_t {
    COOPERATE_STATE_FREE = 0,
    COOPERATE_STATE_OUT,
    COOPERATE_STATE_IN,
    N_COOPERATE_STATES,
};

enum class CooperateEventType {
    NOOP,
    QUIT,
    REGISTER_LISTENER,
    UNREGISTER_LISTENER,
    REGISTER_HOTAREA_LISTENER,
    UNREGISTER_HOTAREA_LISTENER,
    ENABLE,
    DISABLE,
    START,
    STOP,
    GET_COOPERATE_STATE,
    DUMP,
    APP_CLOSED,
    DDM_BOARD_ONLINE,
    DDM_BOARD_OFFLINE,
    DDP_COOPERATE_SWITCH_CHANGED,
    INPUT_HOTPLUG_EVENT,
    INPUT_POINTER_EVENT,
    DSOFTBUS_SESSION_OPEND,
    DSOFTBUS_SESSION_CLOSED,
    DSOFTBUS_START_COOPERATE,
    DSOFTBUS_START_COOPERATE_RESPONSE,
    DSOFTBUS_START_COOPERATE_FINISHED,
    DSOFTBUS_COME_BACK,
    DSOFTBUS_STOP_COOPERATE,
    DSOFTBUS_RELAY_COOPERATE,
    DSOFTBUS_RELAY_COOPERATE_FINISHED,
};

struct Coordinate {
    int32_t x;
    int32_t y;
};
using NormalizedCoordinate = Coordinate;

struct Rectangle {
    int32_t width;
    int32_t height;
    int32_t x;
    int32_t y;
};

struct UpdateStateEvent {
    CooperateState current;
};

struct RegisterListenerEvent {
    int32_t pid;
    int32_t userData;
};

using UnregisterListenerEvent = RegisterListenerEvent;
using RegisterHotareaListenerEvent = RegisterListenerEvent;
using UnregisterHotareaListenerEvent = RegisterListenerEvent;
using EnableCooperateEvent = RegisterListenerEvent;
using DisableCooperateEvent = RegisterListenerEvent;

struct StartCooperateEvent {
    int32_t pid;
    int32_t userData;
    std::string remoteNetworkId;
    int32_t startDeviceId;
};

struct StopCooperateEvent {
    int32_t pid;
    int32_t userData;
    bool isUnchained;
};

struct GetCooperateStateEvent {
    int32_t pid;
    int32_t userData;
    std::string networkId;
};

struct DumpEvent {
    int32_t fd;
};

struct DDMBoardOnlineEvent {
    std::string networkId;
    bool normal;
};

using DDMBoardOfflineEvent = DDMBoardOnlineEvent;
using DDPCooperateSwitchChanged = DDMBoardOnlineEvent;

enum class InputHotplugType {
    PLUG,
    UNPLUG,
};

struct InputHotplugEvent {
    int32_t deviceId;
    InputHotplugType type;
};

struct InputPointerEvent {
    int32_t deviceId;
    int32_t pointerAction;
    int32_t sourceType;
    Coordinate position;
};

using DSoftbusSessionOpened = DDMBoardOnlineEvent;
using DSoftbusSessionClosed = DDMBoardOnlineEvent;
using DSoftbusStartCooperate = DDMBoardOnlineEvent;
using DSoftbusStartCooperateResponse = DDMBoardOnlineEvent;

struct DSoftbusStartCooperateFinished {
    std::string networkId;
    std::string originNetworkId;
    bool success;
    NormalizedCoordinate cursorPos;
};

using DSoftbusComeBack = DSoftbusStartCooperateFinished;
using DSoftbusStopCooperate = DDMBoardOnlineEvent;
using DSoftbusStopCooperateFinished = DDMBoardOnlineEvent;

struct DSoftbusRelayCooperate {
    std::string networkId;
    std::string targetNetworkId;
    bool normal;
};

using DSoftbusRelayCooperateFinished = DSoftbusRelayCooperate;

struct CooperateEvent {
    CooperateEvent() : type(CooperateEventType::QUIT) {}

    explicit CooperateEvent(CooperateEventType ty) : type(ty) {}

    template<typename Event>
    CooperateEvent(CooperateEventType ty, Event ev) : type(ty), event(ev) {}

    CooperateEventType type;
    std::variant<
        UpdateStateEvent,
        RegisterListenerEvent,
        StartCooperateEvent,
        StopCooperateEvent,
        GetCooperateStateEvent,
        DumpEvent,
        DDMBoardOnlineEvent,
        InputHotplugEvent,
        InputPointerEvent,
        DSoftbusStartCooperateFinished,
        DSoftbusRelayCooperate
    > event;
};

inline constexpr int32_t DEFAULT_TIMEOUT { 3000 };
inline constexpr int32_t REPEAT_ONCE { 1 };
inline constexpr int32_t DEFAULT_COOLING_TIME { 100 };
} // namespace Cooperate
} // namespace DeviceStatus
} // namespace Msdp
} // namespace OHOS
#endif // COOPERATE_EVENTS_H
